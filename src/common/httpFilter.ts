/*
 * @Description:  全局异常拦截器-捕获
 * @Date: 2023-03-20 14:45:40
 * @FilePath: \hksj-vued:\03code\nodejs\nest-admin\src\common\httpFilter.ts
 */
import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';

@Catch(HttpException)
export class HttpFilter implements ExceptionFilter {  // implements ExceptionFilter实现类型约束，知道具体做什么
  catch(exception:HttpException, host: ArgumentsHost) {
      const ctx = host.switchToHttp(); // 获取context
      const request = ctx.getRequest<Request>();  // 引入类型Request作为泛型传入，实现提示
      const response = ctx.getResponse<Response>();
      const status = exception.getStatus(); // 获取状态

      response.status(status).json({
        data: exception,
        status, // es6简写形式
        success: false,
        path: request.url,
        time: new Date().getTime(),
      })
  }
}

/*
 * @Description: 全局响应拦截器-数据格式化
 * @Date: 2023-03-20 14:41:28
 * @FilePath: \hksj-vued:\03code\nodejs\nest-admin\src\common\httpResponse.ts
 */
import { Injectable, NestInterceptor, CallHandler } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

// 加强类型提示
interface data<T>{
  data:T
}

@Injectable()
export class HttpResponse<T = any> implements NestInterceptor {
  intercept(context, next: CallHandler):Observable<data<T>> {
    // pipe是rxjs的管道
    return next.handle().pipe(map(data => {
      return {
        data,
        status: 0,
        success: true,
        message: "成功"
      }
    }))
  }
}
/*
 * @Description: 中间件
 * @Date: 2023-03-18 17:30:03
 * @FilePath: \hksj-vued:\03code\nodejs\nest-demo\src\middleware\index.ts
 */
import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class Logger implements NestMiddleware {
  use(req:Request, res:Response, next:NextFunction) {
    console.log('中间件在这里');
    next();
  }
}
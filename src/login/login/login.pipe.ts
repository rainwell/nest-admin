// 这个pipe可以不写！可以直接使用系统自带的验证ValidationPipe

import { ArgumentMetadata, HttpException, HttpStatus, Injectable, PipeTransform } from '@nestjs/common';
// 类实例化函数
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator'; // 导入验证函数

@Injectable()
export class LoginPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    console.log('value: ', value);
    console.log('metadata: ', metadata);
    //value:  { name: 'yuhao', age: 18 }
    //metadata:  { metatype: [class CreateLoginDto], type: 'body', data: undefined }

    // 第一个参数为dto文件的类型，第二个参数为传入值
    const DTO = plainToInstance(metadata.metatype, value); // 反射到类上面
    const errors = await validate(DTO); // 返回一个promise
    console.log('DTO: ', DTO);
    console.log('errors: ', errors);

    // 如果存在错误
    if(errors.length){
      throw new HttpException(errors, HttpStatus.BAD_REQUEST); // 第一个参数传入错误信息，第二个参数是枚举类型报错
    }
    return value;
  }
}

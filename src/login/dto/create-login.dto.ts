import { IsNotEmpty, Length, IsString, IsNumber } from 'class-validator'; // 使用第三方验证器


export class CreateLoginDto {
  @IsNotEmpty() // 验证是否为空
  @IsString({
    message: "需要传入双引号字符串"
  }) // 是否为字符串
  @Length(4, 20, {
    message: "字符串长度为4到20"
  }) // 字符串长度
  name:string;

  @IsNotEmpty()
  @IsNumber()
  age:number;
}
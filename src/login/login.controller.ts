import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { LoginService } from './login.service';
import { CreateLoginDto } from './dto/create-login.dto';
import { UpdateLoginDto } from './dto/update-login.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
// import { LoginPipe } from './login/login.pipe'; // 引入管道验证DTO

@Controller('login')
@ApiBearerAuth()
@ApiTags('登录接口') // swagger分组接口
export class LoginController {
  constructor(private readonly loginService: LoginService) {}

  @Post()  // 管道直接放在装饰器里面@Body(LoginPipe)
  create(@Body() createLoginDto: CreateLoginDto) {
    return this.loginService.create(createLoginDto);
  }

  @Get()
  findAll() {
    return this.loginService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.loginService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateLoginDto: UpdateLoginDto) {
    return this.loginService.update(+id, updateLoginDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.loginService.remove(+id);
  }
}

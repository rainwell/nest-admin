import { Module } from '@nestjs/common';
import { SystemDicService } from './system-dic.service';
import { SystemDicController } from './system-dic.controller';

@Module({
  controllers: [SystemDicController],
  providers: [SystemDicService]
})
export class SystemDicModule {}

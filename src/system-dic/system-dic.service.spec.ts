import { Test, TestingModule } from '@nestjs/testing';
import { SystemDicService } from './system-dic.service';

describe('SystemDicService', () => {
  let service: SystemDicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SystemDicService],
    }).compile();

    service = module.get<SystemDicService>(SystemDicService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

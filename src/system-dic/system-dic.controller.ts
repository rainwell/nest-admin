import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { SystemDicService } from './system-dic.service';
import { CreateSystemDicDto } from './dto/create-system-dic.dto';
import { UpdateSystemDicDto } from './dto/update-system-dic.dto';

@Controller('system-dic')
export class SystemDicController {
  constructor(private readonly systemDicService: SystemDicService) {}

  @Post()
  create(@Body() createSystemDicDto: CreateSystemDicDto) {
    return this.systemDicService.create(createSystemDicDto);
  }

  @Get()
  findAll() {
    return this.systemDicService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.systemDicService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSystemDicDto: UpdateSystemDicDto) {
    return this.systemDicService.update(+id, updateSystemDicDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.systemDicService.remove(+id);
  }
}

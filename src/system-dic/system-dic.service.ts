import { Injectable } from '@nestjs/common';
import { CreateSystemDicDto } from './dto/create-system-dic.dto';
import { UpdateSystemDicDto } from './dto/update-system-dic.dto';

@Injectable()
export class SystemDicService {
  create(createSystemDicDto: CreateSystemDicDto) {
    return 'This action adds a new systemDic';
  }

  findAll() {
    return `This action returns all systemDic`;
  }

  findOne(id: number) {
    return `This action returns a #${id} systemDic`;
  }

  update(id: number, updateSystemDicDto: UpdateSystemDicDto) {
    return `This action updates a #${id} systemDic`;
  }

  remove(id: number) {
    return `This action removes a #${id} systemDic`;
  }
}

import { PartialType } from '@nestjs/mapped-types';
import { CreateSystemDicDto } from './create-system-dic.dto';

export class UpdateSystemDicDto extends PartialType(CreateSystemDicDto) {}

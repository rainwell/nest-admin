import { Test, TestingModule } from '@nestjs/testing';
import { SystemDicController } from './system-dic.controller';
import { SystemDicService } from './system-dic.service';

describe('SystemDicController', () => {
  let controller: SystemDicController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SystemDicController],
      providers: [SystemDicService],
    }).compile();

    controller = module.get<SystemDicController>(SystemDicController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { SystemDicModule } from './system-dic/system-dic.module';
import { UploadModule } from './upload/upload.module';
import { LoginModule } from './login/login.module';
import { SpiderModule } from './spider/spider.module';
import { GuardModule } from './guard/guard.module';

@Module({
  imports: [
    // nest9版本需要使用forRootAsync导入
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: "mysql", // 数据库类型
        username: "root", // 账号
        password: "123456", // 密码
        host: "localhost", // host
        port: 3306, // 端口号
        database: "midway_demo", // 库名
        // entities: [__dirname + '/**/*.entity{.ts,.js}'], //实体文件
        //synchronize:true, //synchronize字段代表是否自动将实体类同步到数据库(开发环境使用)// 如果第一次使用，不存在表，有同步的需求，需要设置自动创建表接口synchronize: true
        retryDelay:500, // 重试连接数据库间隔
        retryAttempts:10, // 重试连接数据库的次数
        autoLoadEntities:true, //如果为true,将自动加载实体 forFeature()方法注册的每个实体都将自动添加到配置对象的实体数组中
      }),
    }),
    UserModule, SystemDicModule, UploadModule, LoginModule, SpiderModule, GuardModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

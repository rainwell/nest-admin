import { Controller, Get, Request } from '@nestjs/common';
import { SpiderService } from './spider.service';

@Controller('spider')
export class SpiderController {
  constructor(private readonly spiderService: SpiderService) {}

  @Get()
  findAll(@Request() req ) { // 使用装饰器获取请求参数，也可以直接使用@query装饰器
    return this.spiderService.findAll();
  }
}

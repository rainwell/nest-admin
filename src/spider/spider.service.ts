import { Injectable } from '@nestjs/common';
import axios from 'axios';
import * as cheerio from 'cheerio';
import * as fs from 'fs';
import * as path from 'path';


@Injectable()
export class SpiderService {

  async findAll() {
    let curSiteImgTotal = 0;
    let urls:string[] = []; // 存储图片地址的url
    const baseUrl = 'https://www.vcg.com';

    const getData = async () => {
      
      const body = await axios.get('https://www.vcg.com/sets/523819427').then(res => res.data);
      const $ = cheerio.load(body);
      curSiteImgTotal = $('#imageContent figure img').length;
      console.log('imageContent: ', curSiteImgTotal);

      $('#imageContent figure img').each(function() {
        // 这里没有使用箭头函数的原因是需要使用this
        urls.push($(this).attr('src'));
      });
    }
    await getData();
    console.log('urls: ', urls);
    return '共有图片：'+curSiteImgTotal;
  }
}

// 角色权限
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core'; // 反射

@Injectable()
export class RoleGuard implements CanActivate {
  // constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // this.reflector.get('role', context.getHandler());
    console.log("经过了守卫");
    return true;
  }
}

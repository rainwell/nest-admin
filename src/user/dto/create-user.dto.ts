import { ApiProperty } from "@nestjs/swagger"; // 参数属性描述

export class CreateUserDto {
  @ApiProperty({example: "余浩"})
  name: string

  @ApiProperty({example: 18})
  age: number
}

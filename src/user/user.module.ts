import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { Logger } from 'src/middleware';
import { User } from './entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([User])], // 关联实体
  controllers: [UserController],
  providers: [UserService], // 注入service
})
export class UserModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    // 还可以有其他的拦截方式，可以使用对象方式拦截具体的方法
    //  consumer.apply(Logger).forRoutes({path:'user',method:RequestMethod.GET})
    consumer.apply(Logger).forRoutes('/v1/user');  
  }
}

import { Controller, Get, Post, Body, Patch, Param, Request, Delete, Query, Headers, HttpCode, Req, Res, Session } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

import * as svgCaptcha from 'svg-captcha';
import { ApiTags, ApiOperation, ApiParam, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';

@Controller({
  path: 'user',
  version: '1',  // 版本控制
})
@ApiBearerAuth() // 加token
@ApiTags('用户接口') // swagger分组接口
export class UserController {
  constructor(private readonly userService: UserService) {}

  // 请求地址http://localhost:3000/v1/user/code
  @Get('code')
  @ApiOperation({summary: "验证码接口", description:"验证码接口" }) // 描述接口信息
  createCode(@Req() req, @Res() res, @Session() session) {
    // 使用插件生成验证码
    const captcha = svgCaptcha.create({
      size: 4,//生成几个验证码
      fontSize: 50, //文字大小
      width: 100,  //宽度
      height: 49,  //高度
      background: '#cc9966',  //背景颜色
    })
    session.code = captcha.text; //存储验证码记录到前端的cookie里面，过期时间在此项目的main.ts文件中设置maxAge
    res.type('image/svg+xml');
    res.send(captcha.data);
  }

  @Post('createUser')
  createUser(@Body() Body, @Session() session) {
    return {
      code: session.code,
    };
  }

  @Post()
  @ApiOperation({summary: "dto描述", description:"dto描述" }) // 描述接口信息
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Get()
  findAll(@Request() req ) { // 使用装饰器获取请求参数，也可以直接使用@query装饰器
    // console.log('req: ', req); // 打印请求参数
    return this.userService.findAll(req.query.name);
  }

  // get动态路由--/v1/user/123456
  @Get(':id')
  @ApiOperation({summary: "get动态路由", description:"get动态路由" }) // 描述接口信息
  @ApiParam({name: 'id', description: "这是一个id", required: true, type: String }) // 描述接口参数
  @ApiResponse({status: 403, description: "这是一个自定义返回值的描述"})
  findOne(@Param('id') id: string, @Headers() headers) {
    console.log('headers: ', headers); // 获取请求头的信息
    return this.userService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(+id);
  }
}

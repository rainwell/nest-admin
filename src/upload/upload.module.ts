import { Module } from '@nestjs/common';
import { UploadService } from './upload.service';
import { UploadController } from './upload.controller';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname,join } from 'path';

@Module({
  imports: [
    MulterModule.register({
      // 上传后图片存放的位置
      storage:diskStorage({
        destination:join(__dirname,"../images"), // 存放位置,运行后会自动在打包文件夹生成一个images文件夹
        filename:(_, file, callback) => {
          const fileName = `${new Date().getTime() + extname(file.originalname)}`; // 重命名-时间戳+源文件后缀
          return callback(null,fileName);
        }
      })
    })
  ],
  controllers: [UploadController],
  providers: [UploadService]
})
export class UploadModule {

}
